/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileOutputStream;


@MultipartConfig
public class updatepodservlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         Part imagePart = request.getPart("image");
        String title = request.getParameter("TITLE");
        String description = request.getParameter("DESCRIPTION");
        String podcastIdParam = request.getParameter("id");
        int podcastId = Integer.parseInt(podcastIdParam);
         
         
        String imageFileName = getFileName(imagePart);
        String imagePath = getServletContext().getRealPath("/images/") + File.separator + imageFileName;
        
        // public static void updatePodcast(int id, String title, String description, String imageFileName) {
        Model.Model.updatePodcast(podcastId, title, description, imageFileName);
        saveFile(imagePart, imagePath);
        response.sendRedirect("MyPodcastsServlet");
    }
    private String getFileName(Part part) {
                 String contentDispositionHeader = part.getHeader("content-disposition");
                 String[] elements = contentDispositionHeader.split(";");
                 for (String element : elements) {
                     if (element.trim().startsWith("filename")) {
                         return element.substring(element.indexOf('=') + 1).trim().replace("\"", "");
                     }
                 }
                 return null;
    }
    private void saveFile(Part part, String path) throws IOException {
                 try (InputStream input = part.getInputStream();
                      OutputStream output = new FileOutputStream(path)) {
                     byte[] buffer = new byte[1024];
                     int bytesRead;
                     while ((bytesRead = input.read(buffer)) != -1) {
                         output.write(buffer, 0, bytesRead);
                     }
                 }
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
