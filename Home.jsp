<%-- 
    Document   : Home
    Created on : May 13, 2024, 9:34:37 PM
    Author     : oussa
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page language="java" %>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Podcasts</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
    <link
      rel="shortcut icon"
      href="img/logo.png"
      type="image/x-icon"
    />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
      crossorigin="anonymous"
    />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
    <script src="js/script.js" defer></script>
  </head>
  <body
    style="
       
      font-family: 'Poppins', sans-serif;
      background: linear-gradient(#131614, #000000);
      background-repeat: no-repeat;
    "
  >
      

    <div class="container-md">
      <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
          <a class="navbar-brand" href="#"
            ><img id="logo" src="img/logo.png" alt=""
            style="max-width: 200px"
                 /></a>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" style="color: white; padding: 0em 1em" aria-current="page" href="index.jsp">Home</a>
              </li>
              
                <%
                    if(session.getAttribute("email") == null){
                %>
                        <li class="nav-item">
                            <a class="nav-link active" style="color: white; padding: 0em 1em" aria-current="page" href="login.jsp">Login</a>
                        </li>
                <%}%>
              
                <%
                    if(session.getAttribute("email") == null){
                %>
                        <li class="nav-item">
                          <a class="nav-link" style="color: white; padding: 0em 1em" href="Register.jsp">Register</a>
                        </li>
                <%}%>
                
              <li class="nav-item">
                <a class="nav-link" style="color: white; padding: 0em 1em" href="MyPodcastsServlet">My Podcasts</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" style="color: white; padding: 0em 1em" href="addPodcast.jsp">Add Podcast</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" style="color: white; padding: 0em 1em" href="profile.jsp">Account</a>
              </li>
              <li class="nav-item">
                <button class="btntoggle nav-link">Dark</button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
        
    
        
      <div class="main">
          
     <% for (Object[] podcast : (List<Object[]>) request.getAttribute("podcasts")) { %>
        <div class="audio audio1 dark" id="<%= podcast[4] %>">
            <img src="images/<%= podcast[0] %>" alt="">
            <h2><%= podcast[1] %></h2>
            <p><%= podcast[2] %></p>
            <audio controls style="width:290px;">
              <source src="audios/<%= podcast[3] %>" type="audio/mpeg">
              Your browser does not support the audio element.
           
          </audio>
              <form action="audiopage.jsp?podcast_image=<%=podcast[0]%>" GET="">
                <input type="hidden" name="podcast_descrption" value="<%= podcast[2] %>">
                <input type="hidden" name="podcast_title" value="<%= podcast[1] %>">  
                <input type="hidden" name="podcast_image" value="<%= podcast[0] %>">
                <input type="hidden" name="podcast_audio" value="<%= podcast[3] %>">
               <input class="btn btn-danger mt-2" style="width:100px;height:33px" type="submit" value="See">
           </form>
        </div>
      <% } %>
      
      </div>
    </div>

  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>


    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
      crossorigin="anonymous"
    ></script>
  </body>
  

</html>
