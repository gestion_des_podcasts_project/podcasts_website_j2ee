<%-- 
    Document   : allusers
    Created on : May 16, 2024, 3:25:04 PM
    Author     : oussa
--%>

<%@page import="Model.User"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Convert | Export html Table to CSV & EXCEL File</title>
    <link rel="stylesheet" type="text/css" href="css/allusers.css">
</head>

<body>
    <main class="table" id="customers_table">
        <section class="table__header">
            
            <h1>Podcasters</h1>
            <a href="index.jsp">Home</a>
            <div class="input-group">
                <input type="search" placeholder="Search Data...">
                <img src="img/search.png" alt="">
            </div>
<!--            <div class="export__file">
                <label for="export-file" class="export__file-btn" title="Export File"></label>
                <input type="checkbox" id="export-file">
                <div class="export__file-options">
                    <label>Export As &nbsp; &#10140;</label>
                    <label for="export-file" id="toPDF">PDF <img src="images/pdf.png" alt=""></label>
                    <label for="export-file" id="toJSON">JSON <img src="images/json.png" alt=""></label>
                    <label for="export-file" id="toCSV">CSV <img src="images/csv.png" alt=""></label>
                    <label for="export-file" id="toEXCEL">EXCEL <img src="images/excel.png" alt=""></label>
                </div>
            </div>-->
        </section>
        <section class="table__body">
            <table>
                <thead>
                    <tr>
                        <th> Name <span class="icon-arrow">&UpArrow;</span></th>
                        <th> Member since <span class="icon-arrow">&UpArrow;</span></th>
                        <th> adresse emai <span class="icon-arrow">&UpArrow;</span></th>
                        <th> see <span class="icon-arrow">&UpArrow;</span></th>
                    </tr>
                </thead>
                <tbody>
                <% 
                List<User> users = (List<User>) request.getAttribute("userList");
                for (User user : users) { 
                %>
                <tr>
                    <td><%= user.getFullName() %></td>
                    <td><%= user.date_since %></td>
                    <td><%= user.getEmail() %></td>
                    <td>
                        <form action="OtherUserPodcast" method="post">
                            <input type="hidden" name="userid" value="<%=user.id%>">
                            <input class="status delivered" style="width:100px;border:none;cursor: pointer" type="submit" value="See">
                        </form>
                    </td>
                </tr>
            <% } %>

                </tbody>
            </table>
        </section>
    </main>
    <script src="js/allusers.js"></script>

</body>
</html>

