import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.RequestDispatcher;

import Controller.Controller;
import Model.User; // Importez la classe User depuis le modèle

public class loginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        String email = request.getParameter("username"); // Renommé "Email" en "email" pour respecter la convention de nommage
       String Email = request.getParameter("username");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        RequestDispatcher dispatcher = null;
        PrintWriter out = response.getWriter();
        out.println(email);
        out.println(password);

        if (Controller.login(email, password)) {
            
            User user = Controller.getUserProfile(email);
           
            session.setAttribute("user", user);
            session.setAttribute("email",Email);
            dispatcher = request.getRequestDispatcher("index.jsp");
        } else {
            dispatcher = request.getRequestDispatcher("login.jsp");
            request.setAttribute("status", "failed");
        }
        dispatcher.forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}