/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileOutputStream;
import Model.Model;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.servlet.http.HttpSession;
/**
 *
 * @author oussa
 */

@MultipartConfig
public class AddPodcast extends HttpServlet {

            @Override
            protected void doPost(HttpServletRequest request, HttpServletResponse response)
                     throws IOException, ServletException {
                 response.setContentType("text/html;charset=UTF-8");

                 // Retrieve form data
                 Part podcastPart = request.getPart("podcast");
                 Part imagePart = request.getPart("image");
                 String title = request.getParameter("title");
                 String description = request.getParameter("description");

                 // Assuming you have access to the user ID
                HttpSession session = request.getSession();
                String email = (String) session.getAttribute("email");
                int userId = Model.getIdUtilisateurByEmail(email); // Change this to the appropriate user ID

                 // Get file names
                 String imageFileName = getFileName(imagePart);
                 String podcastFileName = getFileName(podcastPart);

                 // Set paths for storing files
                 String imagePath = getServletContext().getRealPath("/images/") + File.separator + imageFileName;
                 String audioPath = getServletContext().getRealPath("/audios/") + File.separator + podcastFileName;

                 // Call the insertPodcast method with the retrieved form data
                 Model.insertPodcast(title, userId, imageFileName, description, podcastFileName);

                 // Save files to the respective directories
                 saveFile(imagePart, imagePath);
                 saveFile(podcastPart, audioPath);

                 // Optionally, you can handle the insertion success/failure and send a response
                 response.sendRedirect("index.jsp");
             }

             // Method to get file name from Part
             private String getFileName(Part part) {
                 String contentDispositionHeader = part.getHeader("content-disposition");
                 String[] elements = contentDispositionHeader.split(";");
                 for (String element : elements) {
                     if (element.trim().startsWith("filename")) {
                         return element.substring(element.indexOf('=') + 1).trim().replace("\"", "");
                     }
                 }
                 return null;
             }

             // Method to save file to a directory
             private void saveFile(Part part, String path) throws IOException {
                 try (InputStream input = part.getInputStream();
                      OutputStream output = new FileOutputStream(path)) {
                     byte[] buffer = new byte[1024];
                     int bytesRead;
                     while ((bytesRead = input.read(buffer)) != -1) {
                         output.write(buffer, 0, bytesRead);
                     }
                 }
             }

    
    
}
