/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.RequestDispatcher;
import java.util.List;
import Controller.Controller;

public class MyPodcastsServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Récupérer l'email de l'utilisateur (peut être obtenu à partir de la session ou des paramètres de la requête)
        HttpSession session = request.getSession();
        String email = (String) session.getAttribute("email"); // Remplacez ceci par la façon dont vous obtenez l'email de l'utilisateur
        
        // Appeler la méthode getMyPodcasts() du modèle pour obtenir les podcasts de l'utilisateur
        List<Object[]> myPodcasts = Controller.getMyPodcasts(email);
        
        // Transmettre les podcasts à la page JSP en tant qu'attribut de requête
        request.setAttribute("myPodcasts", myPodcasts);
        
        // Rediriger vers la page JSP
        request.getRequestDispatcher("Mypodcasts.jsp").forward(request, response);
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}