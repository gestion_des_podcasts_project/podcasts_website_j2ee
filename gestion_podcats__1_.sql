-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 17 mai 2024 à 00:32
-- Version du serveur : 10.4.32-MariaDB
-- Version de PHP : 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestion_podcats`
--

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

CREATE TABLE `favoris` (
  `date_add` date DEFAULT current_timestamp(),
  `id_utilisateur` int(11) DEFAULT NULL,
  `id_podcasts` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `podcasts`
--

CREATE TABLE `podcasts` (
  `id_podcasts` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `url_podcast` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `podcasts`
--

INSERT INTO `podcasts` (`id_podcasts`, `title`, `id_utilisateur`, `image`, `description`, `url_podcast`) VALUES
(89, 'Bassem Youssef: Israel-Palestine, Gaza', 11, 'bassim.jpg', 'Bassem Youssef is an Egyptian-American comedian & satirist, referred to as the Jon Stewart of the Arab World.', 'bassim.mp3'),
(91, 'Tech Talk: Insights into Innovation', 11, 'a.jpg', 'Stay updated on the latest trends and breakthroughs in the tech industry, with expert interviews and discussions on innovation.', 'englishWomen.mp3'),
(94, 'Purrfect Paws Podcast', 11, 'cat.jpg', 'Join us on the Purrfect Paws Podcast, where we celebrate our love for cats! Whether you\'re a devoted cat owner, a cat enthusiast.', 'english2.mp3'),
(95, 'Cillian Murphy breaks down the rise', 11, 'Cillian Murphy as Dr_ Edward Grange, pandemic-obsessed scientist who believes what kills you makes you stronger_.jpg', 'Ahead of Peaky Blinders series 5, Cillian Murphy discusses the rise of Tommy Shelby, the lead character he plays in the show who has evolved from war hero.', 'english3.mp3'),
(96, 'How to make a relationship work', 11, 'yasser.jpg', 'Mental health is a very important topic for me and for many people, and I find that one of the most important components of mental health is human relationships.', 'finjann.mp3'),
(97, 'Building psychological strength', 11, 'al9owat.jpg', 'This episode of Without Paper\r\nPresented by: Faisal Abdul Rahman Al-Aql\r\nPrepared by: Munira Jaber Al-Sharifi\r\nSpecial thanks to Muhammad Farwana for his contribution in preparing this episode', 'sindibad.mp3');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `nom_utilisateur` varchar(200) DEFAULT NULL,
  `adress_email` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `date_since` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `nom_utilisateur`, `adress_email`, `password`, `date_since`) VALUES
(9, 'osama', 'osama@gmail.com', 's12shs1626', '2024-05-16'),
(10, 'Osama', 'osama.akeskous@gmail.com', '7c4ab2635bc11858c6c26ccbadba065b', '2024-05-16'),
(11, 'yassine', 'yassine@gmail.com', '5c847bc7d3dc6b368a0c1fff3630538d', '2024-05-16'),
(12, 'Souad', 'souad@gmail.com', 'b8fde10b147106b93a2a1daba516fa98', '2024-05-16'),
(13, 'Laila', 'laila@gmail.com', 'f30618ed64655812746272636a992b95', '2024-05-16'),
(14, 'Ibrahim', 'ibrahim@gmail.com', 'f1c083e61b32d3a9be76bc21266b0648', '2024-05-16'),
(15, 'ahmed', 'ahmed@gmail.com', '9193ce3b31332b03f7d8af056c692b84', '2024-05-16'),
(16, 'fatima', 'fatima@gmail.com', 'b5d5f67b30809413156655abdda382a3', '2024-05-16'),
(17, 'loay', 'loay@gmail.com', '280498e55df34e84e5ae2dae8212c661', '2024-05-16'),
(18, 'hamid', 'hamid@gmail.com', '37fff357c237d67f2365eab6706bc898', '2024-05-16'),
(19, 'saida', 'saida@gmail.com', 'f5cef96d7e57bb60d8ea3457cb735d26', '2024-05-16'),
(20, 'monir', 'monir@gmail.com', '1ff3ccc659687049ed49add3ce12f01f', '2024-05-16');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `favoris`
--
ALTER TABLE `favoris`
  ADD KEY `fk_utilisateur_from_favoris` (`id_utilisateur`),
  ADD KEY `fk_podcasts_from_favoris` (`id_podcasts`);

--
-- Index pour la table `podcasts`
--
ALTER TABLE `podcasts`
  ADD PRIMARY KEY (`id_podcasts`),
  ADD KEY `fk_podcasts` (`id_utilisateur`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id_utilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `podcasts`
--
ALTER TABLE `podcasts`
  MODIFY `id_podcasts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `favoris`
--
ALTER TABLE `favoris`
  ADD CONSTRAINT `fk_podcasts_from_favoris` FOREIGN KEY (`id_podcasts`) REFERENCES `podcasts` (`id_podcasts`),
  ADD CONSTRAINT `fk_utilisateur_from_favoris` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `podcasts`
--
ALTER TABLE `podcasts`
  ADD CONSTRAINT `fk_podcasts` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
