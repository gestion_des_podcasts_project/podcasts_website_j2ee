import Controller.Controller;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OtherUserPodcast extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve the userid parameter from the request
        String userId = request.getParameter("userid");
        
        // Call the Controller method to get the name of the user by ID
        String name = Controller.getUserNameById(Integer.parseInt(userId));
        
        // Call the Controller method to get the podcastList for the specified userId
        List<Object[]> podcastList = Controller.getOtherUserPodcast(Integer.parseInt(userId));

        // Set the name and podcastList as attributes in the request object
        request.setAttribute("name", name);
        request.setAttribute("podcastList", podcastList);

        // Forward the request to the seeuser.jsp page
        request.getRequestDispatcher("/seeuser.jsp").forward(request, response);
    }
}
